import material_palette as mp
import picoexplorer as display
import utime, random, math
from machine import Pin, ADC, PWM

width = display.get_width()
height = display.get_height()
display_buffer = bytearray(width * height * 2)
display.init(display_buffer)

def blk():
	display.set_pen(0, 0, 0)
	display.clear()
	display.update()

def draw_text(colour, msg, left, top, wrap_col, font_size):
	red, green, blue = colour
	display.set_pen(red, green, blue)
	display.text(msg, left, top, wrap_col, font_size)

# title ("W: " + str(width), 0, 55, 210)
# title ("H: " + str(height), 0, 55, 210)
# Both will display 240. I'm assuming that 240 pixels...

# I have expanded the parameters into full names so they
# make more sense rather than using single letters.
def draw_horiz_line(colour, left, top, right, thickness=1):
	'''
	This method will draw a horizontal line based upon
	the length when (right - left) + 1
	'''
	red, green, blue = colour
	display.set_pen(red, green, blue)
	n = right - left + 1
	for i in range(n):
		if thickness == 1:
			display.pixel(left + i, top)
		else:
			for j in range(thickness + 1):
				display.pixel(left + i, top + j)

def draw_vert_line(colour, left, top, bottom, thickness=1):
	'''
	This method will draw a vertical line based upon
	the length when (bottom - top) + 1
	'''
	red, green, blue = colour
	display.set_pen(red, green, blue)
	n = bottom - top + 1
	for i in range(n):
		
		if thickness == 1:
			display.pixel(left, top + i)
		else:
			for j in range(thickness + 1):
				display.pixel(left + j, top + i)

def draw_box(colour, left, top, right, bottom, thickness=1):
	draw_horiz_line(colour, left, top, right, thickness)
	draw_horiz_line(colour, left, bottom, right+thickness, thickness)
	draw_vert_line(colour, left, top, bottom, thickness)
	draw_vert_line(colour, right, top, bottom+thickness, thickness)

def draw_line(colour, x_startpoint, y_startpoint, x_endpoint, y_endpoint, thickness=1):
	'''
	This method allows developers to draw lines from Point A to B
	anywhere within the display's area i.e. between (0, 0) to (240, 240)
		
		Parameters:
			x_startpoint (int): The X coordinate for the start pixel.
			y_startpoint (int): The Y coordinate for the start pixel.
			x_endpoint (int): The X coordinate for the end pixel.
			y_endpoint (int): The Y coordinate for the end pixel.
	'''
	if x_startpoint > x_endpoint:
		temp_x = x_startpoint
		x_startpoint = x_endpoint
		x_endpoint = temp_x
		
		temp_y = y_startpoint
		y_startpoint = y_endpoint
		y_endpoint = temp_y
	
	if x_endpoint - x_startpoint == 0:
		draw_vert_line(colour, x_startpoint, min(y_startpoint, y_endpoint), max(y_startpoint, y_endpoint), thickness=1)
	else:
		red, green, blue = colour
		display.set_pen(red, green, blue)
		n = x_endpoint - x_startpoint + 1
		gradient = float((y_endpoint - y_startpoint) / (x_endpoint - x_startpoint))
		for i in range(n):
			y_pixel = y_startpoint + int(gradient * i)
			if thickness == 1:
				display.pixel(x_startpoint + i, y_pixel)
			else:
				for j in range(thickness):
					display.pixel(x_startpoint + i, y_pixel + j)
		display.update()

def update_screen(delay_sec):
	display.update()
	utime.sleep(delay_sec)

def align_text(display_val, max_chars):
	msg1 = str(display_val)
	space = max_chars - len(msg1)
	msg2 = ""
	for m in range(space):
		msg2 += " "
	msg2 += msg1
	return msg2

def draw_circle(x_center, y_center, radius):
	'''
	Draw a circle by specifying it's center point and radius
	'''
	display.circle(x_center, y_center, radius)
	display.set_pen(0, 0, 0)
	display.circle(x_center, y_center, radius-1)

def draw_arc(x_center, y_center, radius, direction):
	'''
	Thinking about creating a method to allow programmers to draw an arc around a point of 
	origin. Not just that, they can specify the angle and the start point.
	
	Notes:
		i)	The start point obviously being the end of a line if the user wants to start drawing 
			the arc from there.
		ii) With the direction parameter, we can calculate which direction to draw the arc based
			on the origin point i.e. (start point) +/- radius.
	'''
	pass

def showgraph(v):
	display.set_pen(255, 0, 0)
	display.text("V", 8, 50, 240, 3)
	display.set_pen(0, 0, 0)
	display.rectangle(29, 50, 220, 16)
	display.set_pen(200, 200, 0)
	display.rectangle(29, 50, v, 15)
	display.set_pen(255, 255, 255)
	draw_vert_line(mp.WHITE, 28, 46, 68, 1)
	display.set_pen(0, 0, 255)
	display.text(str(align_text(v, 4)) + " %", 140, 48, 240, 3)

def draw_bargraph(border_colour, fill_colour, left, top, bottom, right, value):
	'''
	Okay, so this is a function for drawing both the rectangle [draw_box()], and then specifying 
	the value for the bar graph. So with the length of the rectangle, we calculate the fill area.

	Notes:
		i)	I've added a parameter for the border_color so that users can specify the color of the
			border surrounding the fill area.
		ii)	Users can also specify a different fill_color if needed. So if the value is 40 (40%), 
			we will shade 40% of the rectangle with the fill_color
	'''
	pass

def draw_palette():
	# 1st row
	display.set_pen(244, 67, 54)
	display.rectangle(20, 20, 50, 50)
	display.set_pen(232, 30, 99)
	display.rectangle(70, 20, 50, 50)
	display.set_pen(156, 39, 176)
	display.rectangle(120, 20, 50, 50)
	display.set_pen(103, 58, 183)
	display.rectangle(170, 20, 50, 50)
	# 2nd row
	display.set_pen(63, 81, 181)
	display.rectangle(20, 70, 50, 50)
	display.set_pen(33, 150, 243)
	display.rectangle(70, 70, 50, 50)
	display.set_pen(3, 169, 244)
	display.rectangle(120, 70, 50, 50)
	display.set_pen(0, 188, 212)
	display.rectangle(170, 70, 50, 50)
	# 3rd row
	display.set_pen(0, 150, 136)
	display.rectangle(20, 120, 50, 50)
	display.set_pen(76, 175, 80)
	display.rectangle(70, 120, 50, 50)
	display.set_pen(139, 195, 74)
	display.rectangle(120, 120, 50, 50)
	display.set_pen(205, 220, 57)
	display.rectangle(170, 120, 50, 50)
	# 4th row
	display.set_pen(255, 235, 59)
	display.rectangle(20, 170, 50, 50)
	display.set_pen(255, 193, 7)
	display.rectangle(70, 170, 50, 50)
	display.set_pen(255, 152, 0)
	display.rectangle(120, 170, 50, 50)
	display.set_pen(255, 87, 34)
	display.rectangle(170, 170, 50, 50)

	display.update()
	utime.sleep(5)
	blk()

	# Page 2, 1st row
	display.set_pen(121, 85, 72)
	display.rectangle(20, 20, 50, 50)
	display.set_pen(158, 158, 158)
	display.rectangle(70, 20, 50, 50)
	display.set_pen(96, 125, 139)
	display.rectangle(120, 20, 50, 50)
	display.set_pen(255, 255, 255)
	display.rectangle(170, 20, 50, 50)

smiley = [0x00, 0x0A, 0x00, 0x04, 0x11, 0x0E, 0x00, 0x00]
sad = [0x00, 0x0A, 0x00, 0x04, 0x00, 0x0E, 0x11, 0x00]
heart = [0, 0, 0, 10, 31, 14, 4, 0]
b_heart = [0, 10, 31, 0, 0, 14, 4, 0]
up_arrow = [0, 4, 14, 21, 4, 4, 0, 0]
down_arrow = [0, 4, 4, 21, 14, 4, 0, 0]
bits = [182, 64, 32, 16, 8, 4, 2, 1]	# Powers of 2

def mychar2(xpos, ypos, pattern):
	for line in range(8):
		for ii in range(5):
			i = ii + 3
			dot = pattern[line] & bits[i]
			if dot:
				display.pixel(xpos + i * 2, ypos + line * 2)
				display.pixel(xpos + i * 2, ypos + line * 2 + 1)
				display.pixel(xpos + i * 2 + 1, ypos + line * 2)
				display.pixel(xpos + i * 2 + 1, ypos + line * 2 + 1)

# print("Size of standard palette:", len(mp.standard_palette))
# print("Colour[0]:", mp.standard_palette[0])
# draw_palette()

potentiometer = ADC(28)
led = PWM(Pin(4))
led.freq(1000)
led.duty_u16(0)

blk()
draw_text(mp.CYAN, "Physical Computing with Graphics", 20, 70, 200, 4)
display.update()
utime.sleep(5)
blk()

running = True
display.set_pen(255, 255, 255)
display.text("Turn Potentiometer", 20, 15, 230, 2)
display.set_pen(100, 100, 100)
display.text("Press button Y to halt", 5, 215, 230, 2)
display.set_pen(0, 100, 0)
draw_box(mp.WHITE, 60, 80, 180, 200, 2)
while running:
	pot_raw = potentiometer.read_u16()
	pot = pot_raw / 256
	# Adjust end values: 0 & 255
	pot = int(pot * 256.0/255.0) - 1
	if pot > 255:
		pot = 255
	percent = int(100 * pot / 255)
	showgraph(percent)
	display.update()
	duty = pot_raw - 300
	if duty < 0:
		duty = 0
	led.duty_u16(duty)
	display.set_pen(pot, pot, pot)
	display.circle(120, 140, 50)
	if display.is_pressed(3):
		running = False

blk()

#display.set_pen(0, 200, 0)
#draw_text(mp.GREEN, "Colour Palette", 15, 15, 200, 2)
# character_set = ""
# count = 0
# for i in range(32, 128, 8):
# 	for j in range(0, 8, 1):
# 		p = i + j
# 		if ((p < 97) or (p > 122)):
# 			character_set += chr(p)
# 			count = count + 1
# 			if (count) / 16 == int((count) / 16):
# 				character_set += " "
# print(character_set)
# draw_text(mp.WHITE, character_set, 15, 40, 200, 2)
# draw_text(mp.LIGHT_BLUE, "No lower case", 140, 110, 200, 1)
# draw_text(mp.AMBER, "Size 3", 15, 130, 200, 3)
# draw_text(mp.LIME, "Size 4", 15, 156, 200, 4)
# draw_text(mp.PURPLE, "Size 6", 15, 190, 200, 6)

# blk()
# draw_box(colour=PRIMARY_BLUE, left=0, top=105, right=100, bottom=205, thickness=2)
# mychar2(20, 130, up_arrow)
# mychar2(40, 130, smiley)
# mychar2(60, 130, heart)
# mychar2(20, 160, down_arrow)
# mychar2(40, 160, sad)
# mychar2(60, 160, b_heart)
# update_screen(5)
# blk()

# blk()
# draw_box(colour=PRIMARY_BLUE, left=10, top=10, right=230, bottom=230, thickness=5)
# # draw_line(PRIMARY_BLUE, x_startpoint=200, y_startpoint=100, x_endpoint=70, y_endpoint=30, thickness=1)
# display_text(PRIMARY_TEXT, "Width : " + align_text(str(width), 5), 40, 40, 200, 3)
# display_text(PRIMARY_TEXT, "Height: " + align_text(str(width), 5), 40, 60, 200, 3)
# display.update()
# utime.sleep(5)
# blk()